<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/home.htm */
class __TwigTemplate_14a1017340c3c6bf7ce8f84fec92e1a9dddc7c75880a9b122bd8e2aa75eacc74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "<!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"flexslider heroflex hero-slider\" style='z-index: -1' data-autoplay=\"yes\" data-pagination=\"no\" data-arrows=\"yes\" data-style=\"fade\" data-pause=\"yes\">
    \t\t<video style='z-index: -1;' autoplay=\"autoplay\" muted='muted' loop='loop' width=\"100%\" height=\"auto\" poster=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/videos/drone-poster-00001.jpg");
        echo "\" preload=\"true\">
            \t<source src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/videos/drone-transcode.webm");
        echo "\" type=\"video/webm\"/>
            \t<source src=\"";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/videos/drone-transcode.mp4");
        echo "\" type=\"video/mp4\" />
            \t<source src=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/videos/drone-transcode.oog");
        echo "\" type=\"video/oog\" />
\t\t\t     Your browser does not support the video tag.
\t\t\t </video>
       \t</div>
    </div>
    <div class=\"quote-teaser\">
    \t<div class=\"container\">
        \t<div class=\"quote-teaser-title\">
            \t<h3><i class=\"fa fa-info-circle accent-color \"></i> Lépjen kapcsolatban velünk</h3>
           \t</div>
            <form method=\"post\" id=\"quoteform\" name=\"quoteform\" class=\"quick-quote clearfix\" action=\"mail/quote.php\">
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <input name=\"quote-name\" id=\"quote-name\" type=\"text\" placeholder=\"Neve\" class=\"form-control\">
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input name=\"quote-email\" id=\"quote-email\" type=\"email\" placeholder=\"Email címe\" class=\"form-control\">
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <select name=\"quote-service\" id=\"quote-service\" class=\"form-control selectpicker\">
                            <option>Miben segíthetünk?</option>
                            <option>Növénytermesztés</option>
                            <option>Állattenyésztés</option>
                            <option>Egyéb</option>
                        </select>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input id=\"quote-submit\" name=\"quote-submit\" type=\"submit\" value=\"Küldés\" class=\"yelow-bg btn btn-primary btn-block\">
                    </div>
                </div>
            \t<div id=\"Quote-message\" class=\"accent-color\"></div>
            </form>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"shadow-row\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-6 col-sm-6\">
                            <h1 class=\"short\">Ismerjen meg minket</h1>
                            <p><strong>céljaink</strong></p>
                            <p><img src=\"";
        // line 51
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img2.jpg");
        echo "\" alt=\"\"></p>
                            <p>A Kft. azt a célt tűzte ki maga elé, hogy színvonalas gazdálkodással, korszerű technikai fejlesztésekkel, átgondolt piackutatással olyan szintű középgazdaságot tudjon kialakítani , amely megfelel az EU szabványoknak. </p>
                            <a href=\"";
        // line 53
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/rolunk");
        echo "\" class=\"btn btn-default\">Bővebben rólunk</a>
                        </div>
                        <div class=\"spacer-40 visible-xs\"></div>
                        <div class=\"col-md-6 col-sm-6\">
                            <div class=\"connected-blocks\">
                                <span class=\"icon\"><i class=\"fa fa-male\"></i></span>
                                <h5>Így dolgozunk</h5>
                                <h3>Aenean imperdiet lacus</h3>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada, malesuada iaculis eros dignissim imperdiet lacus sit amet elit porta, et malesuada erat bibendum.</p>
                            </div>
                            <div class=\"connected-blocks\">
                                <span class=\"icon\"><i class=\"fa fa-flag\"></i></span>
                                <h5>Történetünk</h5>
                                <h3>Quisque tempor dolor <a href=\"#\">sit amet</a> tellus malesuada</h3>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada, malesuada iaculis eros dignissim.</p>
                            </div>
                        </div>
                    </div>
                </div>
          \t\t<div class=\"spacer-60\"></div>
            </div>
           \t<div class=\"lgray-bg\" style=\"background-image:url(";
        // line 74
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaves1.png");
        echo "); background-repeat:repeat\">
            \t<div class=\"container\">
                \t<h4 class=\"stacked-title yelow-bg\">Miért mi? </h4>
                    <div class=\"row\">
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-trophy\"></i></span>
                                <h5>Díjnyertes termékek</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-arrow-up\"></i></span>
                                <h5>Magas minőség</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-users\"></i></span>
                                <h5>Konstruktív munka</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-dollar\"></i></span>
                                <h5>Kedvező díjszabás</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"spacer-50\"></div>
               \t</div>
            </div>
            <div class=\"spacer-60\"></div>
        \t    <div id=\"main-container\">
        <div class=\"content\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <h2>Tevékenységek</h2>
                      
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"services-slider\" data-columns=\"3\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"3\" data-items-desktop-small=\"3\" data-items-tablet=\"2\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"";
        // line 123
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img3.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Növénytermesztés</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">Bővebben</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"";
        // line 133
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img4.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Állattenyésztés</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"";
        // line 143
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img5.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Kereskedelem</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"";
        // line 153
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img6.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Bérmunkák</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"spacer-40 visible-sm visible-xs\"></div>
                    <div class=\"col-md-4\">
                        <h2>Gyakran feltett kérdések</h2>
                   
                        <div class=\"spacer-30\"></div>
                        <div class=\"accordion\" id=\"accordionArea\">
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle active\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#oneArea\">Ez egy kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"oneArea\" class=\"accordion-body in collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#twoArea\">Ez meg egy másik kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"twoArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#threeArea\">Ismét egy kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"threeArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#fourArea\">Utolsó kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"fourArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style=\"margin-top:30px\"></div>
        <div  class=\"parallax parallax1 parallax-light padding-tb75\" style=\"background-image:url(images/parallax1.jpg);\">
                <div class=\"text-align-center\">
                    <h2>Számunkra fontos az egészséges élettér<br> és hogy az állataink és növényeink is<br> jól érezzék magukat!</h2>
                    <a href=\"#\" class=\"btn btn-default btn-ghost btn-light\">Kapcsolat</a>
                </div>
        </div>
           
            <div class=\"padding-tb75\">
                <div class=\"container\">
                    <div class=\"text-align-center\">
                        <h2>Partnereink mondták</h2>
                
                    </div>
                    <div class=\"spacer-20\"></div>
                    <div class=\"carousel-wrapper\">
                        <div class=\"row\">
                            <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"3\" data-autoplay=\"\" data-pagination=\"yes\" data-arrows=\"no\" data-single-item=\"no\" data-items-desktop=\"3\" data-items-desktop-small=\"2\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"";
        // line 232
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user1.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Kovács Karcsi</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"";
        // line 245
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user2.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Szabó Sándor</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"";
        // line 258
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user3.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Horváth Ottó</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        // line 273
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  338 => 273,  320 => 258,  304 => 245,  288 => 232,  206 => 153,  193 => 143,  180 => 133,  167 => 123,  115 => 74,  91 => 53,  86 => 51,  40 => 8,  36 => 7,  32 => 6,  28 => 5,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
<!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"flexslider heroflex hero-slider\" style='z-index: -1' data-autoplay=\"yes\" data-pagination=\"no\" data-arrows=\"yes\" data-style=\"fade\" data-pause=\"yes\">
    \t\t<video style='z-index: -1;' autoplay=\"autoplay\" muted='muted' loop='loop' width=\"100%\" height=\"auto\" poster=\"{{'assets/videos/drone-poster-00001.jpg'|theme}}\" preload=\"true\">
            \t<source src=\"{{'assets/videos/drone-transcode.webm'|theme}}\" type=\"video/webm\"/>
            \t<source src=\"{{'assets/videos/drone-transcode.mp4'|theme}}\" type=\"video/mp4\" />
            \t<source src=\"{{'assets/videos/drone-transcode.oog'|theme}}\" type=\"video/oog\" />
\t\t\t     Your browser does not support the video tag.
\t\t\t </video>
       \t</div>
    </div>
    <div class=\"quote-teaser\">
    \t<div class=\"container\">
        \t<div class=\"quote-teaser-title\">
            \t<h3><i class=\"fa fa-info-circle accent-color \"></i> Lépjen kapcsolatban velünk</h3>
           \t</div>
            <form method=\"post\" id=\"quoteform\" name=\"quoteform\" class=\"quick-quote clearfix\" action=\"mail/quote.php\">
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <input name=\"quote-name\" id=\"quote-name\" type=\"text\" placeholder=\"Neve\" class=\"form-control\">
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input name=\"quote-email\" id=\"quote-email\" type=\"email\" placeholder=\"Email címe\" class=\"form-control\">
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <select name=\"quote-service\" id=\"quote-service\" class=\"form-control selectpicker\">
                            <option>Miben segíthetünk?</option>
                            <option>Növénytermesztés</option>
                            <option>Állattenyésztés</option>
                            <option>Egyéb</option>
                        </select>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input id=\"quote-submit\" name=\"quote-submit\" type=\"submit\" value=\"Küldés\" class=\"yelow-bg btn btn-primary btn-block\">
                    </div>
                </div>
            \t<div id=\"Quote-message\" class=\"accent-color\"></div>
            </form>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"shadow-row\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-6 col-sm-6\">
                            <h1 class=\"short\">Ismerjen meg minket</h1>
                            <p><strong>céljaink</strong></p>
                            <p><img src=\"{{'assets/images/img2.jpg'|theme}}\" alt=\"\"></p>
                            <p>A Kft. azt a célt tűzte ki maga elé, hogy színvonalas gazdálkodással, korszerű technikai fejlesztésekkel, átgondolt piackutatással olyan szintű középgazdaságot tudjon kialakítani , amely megfelel az EU szabványoknak. </p>
                            <a href=\"{{'/rolunk'|app}}\" class=\"btn btn-default\">Bővebben rólunk</a>
                        </div>
                        <div class=\"spacer-40 visible-xs\"></div>
                        <div class=\"col-md-6 col-sm-6\">
                            <div class=\"connected-blocks\">
                                <span class=\"icon\"><i class=\"fa fa-male\"></i></span>
                                <h5>Így dolgozunk</h5>
                                <h3>Aenean imperdiet lacus</h3>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada, malesuada iaculis eros dignissim imperdiet lacus sit amet elit porta, et malesuada erat bibendum.</p>
                            </div>
                            <div class=\"connected-blocks\">
                                <span class=\"icon\"><i class=\"fa fa-flag\"></i></span>
                                <h5>Történetünk</h5>
                                <h3>Quisque tempor dolor <a href=\"#\">sit amet</a> tellus malesuada</h3>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada, malesuada iaculis eros dignissim.</p>
                            </div>
                        </div>
                    </div>
                </div>
          \t\t<div class=\"spacer-60\"></div>
            </div>
           \t<div class=\"lgray-bg\" style=\"background-image:url({{'assets/images/leaves1.png'|theme}}); background-repeat:repeat\">
            \t<div class=\"container\">
                \t<h4 class=\"stacked-title yelow-bg\">Miért mi? </h4>
                    <div class=\"row\">
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-trophy\"></i></span>
                                <h5>Díjnyertes termékek</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-arrow-up\"></i></span>
                                <h5>Magas minőség</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-users\"></i></span>
                                <h5>Konstruktív munka</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-6\">
                            <div class=\"feature-block\">
                                <span class=\"icon\"><i class=\"fa fa-dollar\"></i></span>
                                <h5>Kedvező díjszabás</h5>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"spacer-50\"></div>
               \t</div>
            </div>
            <div class=\"spacer-60\"></div>
        \t    <div id=\"main-container\">
        <div class=\"content\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8\">
                        <h2>Tevékenységek</h2>
                      
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"services-slider\" data-columns=\"3\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"3\" data-items-desktop-small=\"3\" data-items-tablet=\"2\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"{{'assets/images/img3.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Növénytermesztés</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">Bővebben</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"{{'assets/images/img4.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Állattenyésztés</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"{{'assets/images/img5.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Kereskedelem</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"service-grid-item grid-item format-standard\">
                                            <a href=\"service-single.html\" class=\"media-box\"><img src=\"{{'assets/images/img6.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h4><a href=\"service-single.html\">Bérmunkák</a></h4>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</p>
                                                <a href=\"service-single.html\" class=\"more\">View service details</a>
                                            </div>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class=\"spacer-40 visible-sm visible-xs\"></div>
                    <div class=\"col-md-4\">
                        <h2>Gyakran feltett kérdések</h2>
                   
                        <div class=\"spacer-30\"></div>
                        <div class=\"accordion\" id=\"accordionArea\">
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle active\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#oneArea\">Ez egy kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"oneArea\" class=\"accordion-body in collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#twoArea\">Ez meg egy másik kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"twoArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#threeArea\">Ismét egy kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"threeArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                            <div class=\"accordion-group panel\">
                                <div class=\"accordion-heading accordionize\">
                                    <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordionArea\" href=\"#fourArea\">Utolsó kérdés<i class=\"fa fa-angle-down\"></i> </a>
                                </div>
                                <div id=\"fourArea\" class=\"accordion-body collapse\">
                                    <div class=\"accordion-inner\">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style=\"margin-top:30px\"></div>
        <div  class=\"parallax parallax1 parallax-light padding-tb75\" style=\"background-image:url(images/parallax1.jpg);\">
                <div class=\"text-align-center\">
                    <h2>Számunkra fontos az egészséges élettér<br> és hogy az állataink és növényeink is<br> jól érezzék magukat!</h2>
                    <a href=\"#\" class=\"btn btn-default btn-ghost btn-light\">Kapcsolat</a>
                </div>
        </div>
           
            <div class=\"padding-tb75\">
                <div class=\"container\">
                    <div class=\"text-align-center\">
                        <h2>Partnereink mondták</h2>
                
                    </div>
                    <div class=\"spacer-20\"></div>
                    <div class=\"carousel-wrapper\">
                        <div class=\"row\">
                            <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"3\" data-autoplay=\"\" data-pagination=\"yes\" data-arrows=\"no\" data-single-item=\"no\" data-items-desktop=\"3\" data-items-desktop-small=\"2\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user1.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Kovács Karcsi</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user2.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Szabó Sándor</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class=\"item\">
                                    <div class=\"testimonial-block\">
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                        </blockquote>
                                        <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user3.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                        <div class=\"testimonial-info\">
                                            <div class=\"testimonial-info-in\">
                                                <strong>Horváth Ottó</strong><span></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/home.htm", "");
    }
}
