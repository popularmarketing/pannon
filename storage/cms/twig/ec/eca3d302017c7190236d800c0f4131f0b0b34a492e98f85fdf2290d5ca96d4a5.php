<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blogpost.htm */
class __TwigTemplate_e6dac9b5ea46fda897242e4862e18cd96947a6d2523944bda879b32885475b4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\">Főoldal</a></li>
                    <li> <a href=\"";
        // line 8
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/hirek");
        echo "\" class=\"active\">Hírek</a></li>
                </ol>
            \t<h1>Hírek</h1>
            </div>
        </div>
    </div>
";
        // line 14
        $context["post"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["blogPost"] ?? null), "post", array());
        // line 15
        echo "    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8\">
                    \t<h2>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "title", array()), "html", null, true);
        echo "</h2>
                    \t<div class=\"post-media\">
                    \t   ";
        // line 23
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "featured_images", array()), "count", array())) {
            // line 24
            echo "                    \t       ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "featured_images", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 25
                echo "                    \t           <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "path", array()), "html", null, true);
                echo "\" alt=\"image.title != null ? image.title : post.title\">       
                    \t       ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                    \t   ";
        } else {
            // line 28
            echo "                    \t       <img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img9.jpg");
            echo "\" alt=\"\">
                    \t   ";
        }
        // line 30
        echo "                        </div>
                        <div class=\"blog-item-meta\">
                        \t<div class=\"meta-data\"><i class=\"fa fa-clock-o\"></i> ";
        // line 32
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "published_at", array()), "Y-M-d"), "html", null, true);
        echo "</div>
                            <div class=\"meta-data\"><a href=\"#\">Pannon Kft.</a></div>
                        </div>
                        <div class=\"post-content\">
                        \t";
        // line 36
        echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "content_html", array());
        echo "
                      \t</div>
                    </div>
                    <div class=\"col-lg-3 col-md-4\">
                        <div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t";
        // line 42
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticMenu2"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 43
        echo "                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"";
        // line 49
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/kapcsolat");
        echo "\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   \t</div>
";
        // line 57
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blogpost.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 57,  115 => 49,  107 => 43,  103 => 42,  94 => 36,  87 => 32,  83 => 30,  77 => 28,  74 => 27,  65 => 25,  60 => 24,  58 => 23,  53 => 21,  45 => 15,  43 => 14,  34 => 8,  30 => 7,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"{{''|app}}\">Főoldal</a></li>
                    <li> <a href=\"{{'/hirek'|app}}\" class=\"active\">Hírek</a></li>
                </ol>
            \t<h1>Hírek</h1>
            </div>
        </div>
    </div>
{% set post = blogPost.post %}
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8\">
                    \t<h2>{{ post.title }}</h2>
                    \t<div class=\"post-media\">
                    \t   {% if post.featured_images.count %}
                    \t       {% for image in post.featured_images|slice(0, 1) %}
                    \t           <img src=\"{{image.path}}\" alt=\"image.title != null ? image.title : post.title\">       
                    \t       {% endfor %}
                    \t   {% else %}
                    \t       <img src=\"{{'assets/images/img9.jpg'|theme}}\" alt=\"\">
                    \t   {% endif %}
                        </div>
                        <div class=\"blog-item-meta\">
                        \t<div class=\"meta-data\"><i class=\"fa fa-clock-o\"></i> {{ post.published_at|date('Y-M-d') }}</div>
                            <div class=\"meta-data\"><a href=\"#\">Pannon Kft.</a></div>
                        </div>
                        <div class=\"post-content\">
                        \t{{ post.content_html|raw }}
                      \t</div>
                    </div>
                    <div class=\"col-lg-3 col-md-4\">
                        <div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t{%component 'staticMenu2' %}
                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"{{'/kapcsolat'|app}}\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   \t</div>
{% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blogpost.htm", "");
    }
}
