<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/contact.htm */
class __TwigTemplate_48df63753b878ecb125b1fd8e7ec962898b3c79000c9b9929b9f155ca013d365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\">Főoldal</a></li>
                    <li class=\"";
        // line 10
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("kapcsolat");
        echo "\">Kapcsolat</li>
                </ol>
            \t<h1>Elérhetőségek</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8 col-sm-7\">
                    \t<h3>Lépjen kapcsolatba velünk</h3>
            \t\t\t<p>Bármilyen kérdés felmerülése esetén munkatársaink munkaidőben a lehető leggyorsabb időn belül igyekeznek választ adni.</p>
                    \t<form method=\"post\" id=\"contactform\" name=\"contactform\" class=\"contact-form clearfix\" action=\"mail/contact.php\">
                            <div class=\"row\">
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Név (kötelező)</label>
                                    <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control input-lg\">
                                </div>
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Email cím (kötelező)</label>
                                    <input type=\"email\" id=\"email\" name=\"email\" class=\"form-control input-lg\">
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Telefonszám (kötelező)</label>
                                    <input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control input-lg\">
                                </div>
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Tárgy</label>
                                    <input type=\"text\" id=\"subject\" name=\"subject\" class=\"form-control input-lg\">
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <label>Üzenet</label>
                                    <textarea class=\"form-control input-lg\" id=\"comments\" name=\"comments\" rows=\"6\"></textarea>
                                    
                                    <button type=\"submit\" id=\"submit\" name=\"submit\" class=\"btn btn-primary btn-lg\">Küldés</button>
                                </div>
                            </div>
                        </form>
                        <div class=\"clearfix\"></div>
                        <div id=\"message\"></div>
                  \t</div>
                    <div class=\"spacer-40 visible-xs\"></div>
                    <div class=\"col-lg-3 col-md-4 col-sm-5\">
                        <div class=\"accent-bg text_banner\">
                            <h4 class=\"short\">Nyitvatartási idő</h4>
                            <ul class=\"working_hours\">
                                <li>
                                    <span>Hétfő</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Kedd</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Szerda</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Csütörtök</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Péntek</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Szombat</span>
                                    <strong>Zárva</strong>
                                </li>
                                <li>
                                    <span>Vasárnap</span>
                                    <strong>Zárva</strong>
                                </li>
                            </ul>
                        </div>
                        <div class=\"feature-block\">
                            <h5>Elérhetőségek</h5>
                            <p>6500 Baja<br>Petőfi Sándor út 80</p><br>
                            <p><strong>Telefonszám:</strong><br><span class=\"accent-color\">+36 79 324-628</span></p><br>
                        </div>
                    </div>
               \t</div>
            </div>
            <div class=\"spacer-60\"></div>
           \t<div id=\"contact-map\"></div>
            <div class=\"spacer-50\"></div>
        \t<div class=\"container\">
            \t<h5 class=\"short\">Munkatársaink elérhetőségei</h5>
                <hr class=\"fw\">
                <div class=\"row\">
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Sales</h2>
                            <p>John Bason</p>
                            <a href=\"mailto:sales@greenskeeper.com\">sales@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Services</h2>
                            <p>Nicole Elmes</p>
                            <a href=\"mailto:services@greenskeeper.com\">services@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Accounts</h2>
                            <p>Christine Harvey</p>
                            <a href=\"mailto:accounts@greenskeeper.com\">accounts@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Store</h2>
                            <p>Nicholas Carter</p>
                            <a href=\"mailto:store@greenskeeper.com\">store@greenskeeper.com</a>
                        </div>
                    </div>
                </div>
          \t</div>
        </div>
   \t</div>
   \t<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAMnbQZbSvJ_-qftCCmb6nkHc6ZqclWfR8\"></script> <!-- Google maps api -->
        <script type=\"text/javascript\">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 17,

\t\t\t\t\tscrollwheel: false,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(46.1737096, 18.9554959), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{\"featureType\":\"landscape\",\"stylers\":[{\"hue\":\"#FFBB00\"},{\"saturation\":43.400000000000006},{\"lightness\":37.599999999999994},{\"gamma\":1}]},{\"featureType\":\"road.highway\",\"stylers\":[{\"hue\":\"#FFC200\"},{\"saturation\":-61.8},{\"lightness\":45.599999999999994},{\"gamma\":1}]},{\"featureType\":\"road.arterial\",\"stylers\":[{\"hue\":\"#FF0300\"},{\"saturation\":-100},{\"lightness\":51.19999999999999},{\"gamma\":1}]},{\"featureType\":\"road.local\",\"stylers\":[{\"hue\":\"#FF0300\"},{\"saturation\":-100},{\"lightness\":52},{\"gamma\":1}]},{\"featureType\":\"water\",\"stylers\":[{\"hue\":\"#0078FF\"},{\"saturation\":-13.200000000000003},{\"lightness\":2.4000000000000057},{\"gamma\":1}]},{\"featureType\":\"poi\",\"stylers\":[{\"hue\":\"#00FF6A\"},{\"saturation\":-1.0989010989011234},{\"lightness\":11.200000000000017},{\"gamma\":1}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id=\"map\" seen below in the <body>
                var mapElement = document.getElementById('contact-map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'GreensKeeper'
                });
            }
        </script>
";
        // line 175
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 175,  36 => 10,  32 => 9,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"{{''|app}}\">Főoldal</a></li>
                    <li class=\"{{'kapcsolat'|app}}\">Kapcsolat</li>
                </ol>
            \t<h1>Elérhetőségek</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8 col-sm-7\">
                    \t<h3>Lépjen kapcsolatba velünk</h3>
            \t\t\t<p>Bármilyen kérdés felmerülése esetén munkatársaink munkaidőben a lehető leggyorsabb időn belül igyekeznek választ adni.</p>
                    \t<form method=\"post\" id=\"contactform\" name=\"contactform\" class=\"contact-form clearfix\" action=\"mail/contact.php\">
                            <div class=\"row\">
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Név (kötelező)</label>
                                    <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control input-lg\">
                                </div>
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Email cím (kötelező)</label>
                                    <input type=\"email\" id=\"email\" name=\"email\" class=\"form-control input-lg\">
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Telefonszám (kötelező)</label>
                                    <input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control input-lg\">
                                </div>
                                <div class=\"col-md-6 col-sm-6\">
                                    <label>Tárgy</label>
                                    <input type=\"text\" id=\"subject\" name=\"subject\" class=\"form-control input-lg\">
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <label>Üzenet</label>
                                    <textarea class=\"form-control input-lg\" id=\"comments\" name=\"comments\" rows=\"6\"></textarea>
                                    
                                    <button type=\"submit\" id=\"submit\" name=\"submit\" class=\"btn btn-primary btn-lg\">Küldés</button>
                                </div>
                            </div>
                        </form>
                        <div class=\"clearfix\"></div>
                        <div id=\"message\"></div>
                  \t</div>
                    <div class=\"spacer-40 visible-xs\"></div>
                    <div class=\"col-lg-3 col-md-4 col-sm-5\">
                        <div class=\"accent-bg text_banner\">
                            <h4 class=\"short\">Nyitvatartási idő</h4>
                            <ul class=\"working_hours\">
                                <li>
                                    <span>Hétfő</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Kedd</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Szerda</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Csütörtök</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Péntek</span>
                                    <strong>06:00 - 16:00</strong>
                                </li>
                                <li>
                                    <span>Szombat</span>
                                    <strong>Zárva</strong>
                                </li>
                                <li>
                                    <span>Vasárnap</span>
                                    <strong>Zárva</strong>
                                </li>
                            </ul>
                        </div>
                        <div class=\"feature-block\">
                            <h5>Elérhetőségek</h5>
                            <p>6500 Baja<br>Petőfi Sándor út 80</p><br>
                            <p><strong>Telefonszám:</strong><br><span class=\"accent-color\">+36 79 324-628</span></p><br>
                        </div>
                    </div>
               \t</div>
            </div>
            <div class=\"spacer-60\"></div>
           \t<div id=\"contact-map\"></div>
            <div class=\"spacer-50\"></div>
        \t<div class=\"container\">
            \t<h5 class=\"short\">Munkatársaink elérhetőségei</h5>
                <hr class=\"fw\">
                <div class=\"row\">
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Sales</h2>
                            <p>John Bason</p>
                            <a href=\"mailto:sales@greenskeeper.com\">sales@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Services</h2>
                            <p>Nicole Elmes</p>
                            <a href=\"mailto:services@greenskeeper.com\">services@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Accounts</h2>
                            <p>Christine Harvey</p>
                            <a href=\"mailto:accounts@greenskeeper.com\">accounts@greenskeeper.com</a>
                        </div>
                    </div>
                \t<div class=\"col-md-3 col-sm-6\">
                    \t<div class=\"feature-text\">
                    \t\t<h2>Store</h2>
                            <p>Nicholas Carter</p>
                            <a href=\"mailto:store@greenskeeper.com\">store@greenskeeper.com</a>
                        </div>
                    </div>
                </div>
          \t</div>
        </div>
   \t</div>
   \t<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAMnbQZbSvJ_-qftCCmb6nkHc6ZqclWfR8\"></script> <!-- Google maps api -->
        <script type=\"text/javascript\">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 17,

\t\t\t\t\tscrollwheel: false,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(46.1737096, 18.9554959), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{\"featureType\":\"landscape\",\"stylers\":[{\"hue\":\"#FFBB00\"},{\"saturation\":43.400000000000006},{\"lightness\":37.599999999999994},{\"gamma\":1}]},{\"featureType\":\"road.highway\",\"stylers\":[{\"hue\":\"#FFC200\"},{\"saturation\":-61.8},{\"lightness\":45.599999999999994},{\"gamma\":1}]},{\"featureType\":\"road.arterial\",\"stylers\":[{\"hue\":\"#FF0300\"},{\"saturation\":-100},{\"lightness\":51.19999999999999},{\"gamma\":1}]},{\"featureType\":\"road.local\",\"stylers\":[{\"hue\":\"#FF0300\"},{\"saturation\":-100},{\"lightness\":52},{\"gamma\":1}]},{\"featureType\":\"water\",\"stylers\":[{\"hue\":\"#0078FF\"},{\"saturation\":-13.200000000000003},{\"lightness\":2.4000000000000057},{\"gamma\":1}]},{\"featureType\":\"poi\",\"stylers\":[{\"hue\":\"#00FF6A\"},{\"saturation\":-1.0989010989011234},{\"lightness\":11.200000000000017},{\"gamma\":1}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id=\"map\" seen below in the <body>
                var mapElement = document.getElementById('contact-map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'GreensKeeper'
                });
            }
        </script>
{% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/contact.htm", "");
    }
}
