<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/about.htm */
class __TwigTemplate_12abc73535d87b4ad13f1db145e7f78d24c5121e241f4b4df9634797e35d8b02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/rolunk");
        echo "\">Főoldal</a></li>
                    <li class=\"active\">Rólunk</li>
                </ol>
            \t<h1>Rövid bemutatkozásunk</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content padding-b0\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-md-9 col-sm-7\">
                    \t";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 23
        echo "                        <div class=\"spacer-30\"></div>
                        ";
        // line 24
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("rmunkasok"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 25
        echo "                    </div>
                    <div class=\"col-md-3 col-sm-5\">
                        <div class=\"widget sidebar-widget text_widget\">
                            <div class=\"feature-block\">
                                <h5>Kapcsolati információ</h5>
                                <p>6500 Baja<br>Petőfi Sándor út 80.</p><br>
                                <p><strong>Hívjon minket</strong><br><span class=\"accent-color\">+36 79 324-628</span></p><br>
                            </div>
                       \t</div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<a href=\"#\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-file-pdf-o fa-2x\"></i> Katalógusunk letöltése</a>
                        </div>
                        <div class=\"widget sidebar-widget\">
                            <div class=\"carousel-wrapper\">
                                <div class=\"row\">
                                    <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"1\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"1\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                        <li class=\"item\">
                                            <div class=\"testimonial-block\">
                                                <blockquote>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                                </blockquote>
                                                <div class=\"testimonial-avatar\"><img src=\"";
        // line 46
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user1.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                                <div class=\"testimonial-info\">
                                                    <div class=\"testimonial-info-in\">
                                                        <strong>Kovács Krisztián</strong><span>Csoma</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class=\"item\">
                                            <div class=\"testimonial-block\">
                                                <blockquote>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                                </blockquote>
                                                <div class=\"testimonial-avatar\"><img src=\"";
        // line 59
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user2.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                                <div class=\"testimonial-info\">
                                                    <div class=\"testimonial-info-in\">
                                                        <strong>Magyar Bálint</strong><span>Dunaföldvár</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           \t</div>
            <div class=\"spacer-40\"></div>
        </div>
   \t</div>
";
        // line 77
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 77,  96 => 59,  80 => 46,  57 => 25,  53 => 24,  50 => 23,  48 => 22,  32 => 9,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"{{'/rolunk'|app}}\">Főoldal</a></li>
                    <li class=\"active\">Rólunk</li>
                </ol>
            \t<h1>Rövid bemutatkozásunk</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content padding-b0\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-md-9 col-sm-7\">
                    \t{% page %}
                        <div class=\"spacer-30\"></div>
                        {% partial 'rmunkasok' %}
                    </div>
                    <div class=\"col-md-3 col-sm-5\">
                        <div class=\"widget sidebar-widget text_widget\">
                            <div class=\"feature-block\">
                                <h5>Kapcsolati információ</h5>
                                <p>6500 Baja<br>Petőfi Sándor út 80.</p><br>
                                <p><strong>Hívjon minket</strong><br><span class=\"accent-color\">+36 79 324-628</span></p><br>
                            </div>
                       \t</div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<a href=\"#\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-file-pdf-o fa-2x\"></i> Katalógusunk letöltése</a>
                        </div>
                        <div class=\"widget sidebar-widget\">
                            <div class=\"carousel-wrapper\">
                                <div class=\"row\">
                                    <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"1\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"1\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                        <li class=\"item\">
                                            <div class=\"testimonial-block\">
                                                <blockquote>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                                </blockquote>
                                                <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user1.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                                <div class=\"testimonial-info\">
                                                    <div class=\"testimonial-info-in\">
                                                        <strong>Kovács Krisztián</strong><span>Csoma</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class=\"item\">
                                            <div class=\"testimonial-block\">
                                                <blockquote>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                                </blockquote>
                                                <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user2.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                                <div class=\"testimonial-info\">
                                                    <div class=\"testimonial-info-in\">
                                                        <strong>Magyar Bálint</strong><span>Dunaföldvár</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           \t</div>
            <div class=\"spacer-40\"></div>
        </div>
   \t</div>
{% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/about.htm", "");
    }
}
