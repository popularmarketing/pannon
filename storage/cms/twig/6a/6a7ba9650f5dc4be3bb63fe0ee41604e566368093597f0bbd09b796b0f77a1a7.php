<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blog.htm */
class __TwigTemplate_ef9ecbeb0ba94882c9e44790dede05d344dc9b46cc5375c6aef265721a97710e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\">Főoldal</a></li>
                    <li class=\"active\">Hírek</li>
                </ol>
            \t<h1>Hírek</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8\">
                            ";
        // line 20
        $context["posts"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["blogPosts"] ?? null), "posts", array());
        // line 21
        echo "                                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 22
            echo "                                        <div class=\"blog-list-item format-standard\">
                                        <div class=\"row\">
                                            <div class=\"col-md-5 col-sm-5\">
                                                <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\" class=\"media-box\">
                                                    ";
            // line 26
            if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "featured_images", array()), "count", array())) {
                // line 27
                echo "                                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "featured_images", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 28
                    echo "                                                            <img src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "path", array()), "html", null, true);
                    echo "\" alt=\"image.title != null ? image.title : post.title\">       
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "                                                    ";
            } else {
                // line 31
                echo "                                                        <img src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img9.jpg");
                echo "\"
                                                        alt=\"\">
                                                    ";
            }
            // line 34
            echo "                                                    <span class=\"date\">
                                                        <span class=\"day\">";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "published_at", array()), "d"), "html", null, true);
            echo "</span>
                                                        <span class=\"month\">";
            // line 36
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "published_at", array()), "M"), "html", null, true);
            echo "</span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class=\"col-md-7 col-sm-7\">
                                                <h3 class=\"blog-title\"><a href=\"";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "title", array()), "html", null, true);
            echo "</a></h3>
                                                <div class=\"blog-item-meta\">
                                                    <div class=\"meta-data\"><a href=\"#\">Pannon Kft.</a></div>
                                                </div>
                                    \t\t\t<p>";
            // line 45
            echo twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "summary", array());
            echo "</p>
                                                <a href=\"";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\" class=\"basic-link\">Tovább a hírhez</a>
                                            </div>
                                        </div>
                                        </div>
                                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 51
            echo "                                        <div class=\"col-md-12 col-sm-12\">
                                            <div class='alert alert-danger'>";
            // line 52
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "</div>
                                        </div>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                                </ul>
                        <!-- Page Pagination -->
                            ";
        // line 57
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "lastPage", array()) > 1)) {
            // line 58
            echo "                                <nav>
                                    <ul class=\"pagination pagination-lg\">
                                        ";
            // line 60
            if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "currentPage", array()) > 1)) {
                // line 61
                echo "                                            <li><a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "currentPage", array()) - 1)));
                echo "\">&larr; Előző</a></li>
                                        ";
            }
            // line 63
            echo "                                
                                        ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "lastPage", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 65
                echo "                                            <li class=\"";
                echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "currentPage", array()) == $context["page"])) ? ("active") : (null));
                echo "\">
                                                <a href=\"";
                // line 66
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => $context["page"]));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
                                            </li>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "                                
                                        ";
            // line 70
            if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "currentPage", array()))) {
                // line 71
                echo "                                            <li><a href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["posts"] ?? null), "currentPage", array()) + 1)));
                echo "\">Következő &rarr;</a></li>
                                        ";
            }
            // line 73
            echo "                                    </nav>
                                ";
        }
        // line 75
        echo "                    </div>
                    <div class=\"col-lg-3 col-md-4\">
                        <div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t";
        // line 79
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticMenu2"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 80
        echo "                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"";
        // line 86
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/kapcsolat");
        echo "\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   \t</div>
";
        // line 94
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 94,  213 => 86,  205 => 80,  201 => 79,  195 => 75,  191 => 73,  185 => 71,  183 => 70,  180 => 69,  169 => 66,  164 => 65,  160 => 64,  157 => 63,  151 => 61,  149 => 60,  145 => 58,  143 => 57,  139 => 55,  130 => 52,  127 => 51,  117 => 46,  113 => 45,  104 => 41,  96 => 36,  92 => 35,  89 => 34,  82 => 31,  79 => 30,  70 => 28,  65 => 27,  63 => 26,  59 => 25,  54 => 22,  48 => 21,  46 => 20,  30 => 7,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
    <div class=\"hero-area\">
    \t<div class=\"page-header dark\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"{{''|app}}\">Főoldal</a></li>
                    <li class=\"active\">Hírek</li>
                </ol>
            \t<h1>Hírek</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-lg-9 col-md-8\">
                            {% set posts = blogPosts.posts %}
                                    {% for post in posts %}
                                        <div class=\"blog-list-item format-standard\">
                                        <div class=\"row\">
                                            <div class=\"col-md-5 col-sm-5\">
                                                <a href=\"{{ post.url }}\" class=\"media-box\">
                                                    {% if post.featured_images.count %}
                                                        {% for image in post.featured_images|slice(0, 1) %}
                                                            <img src=\"{{image.path}}\" alt=\"image.title != null ? image.title : post.title\">       
                                                        {% endfor %}
                                                    {% else %}
                                                        <img src=\"{{'assets/images/img9.jpg'|theme}}\"
                                                        alt=\"\">
                                                    {% endif %}
                                                    <span class=\"date\">
                                                        <span class=\"day\">{{ post.published_at|date('d') }}</span>
                                                        <span class=\"month\">{{ post.published_at|date('M') }}</span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class=\"col-md-7 col-sm-7\">
                                                <h3 class=\"blog-title\"><a href=\"{{ post.url }}\">{{ post.title }}</a></h3>
                                                <div class=\"blog-item-meta\">
                                                    <div class=\"meta-data\"><a href=\"#\">Pannon Kft.</a></div>
                                                </div>
                                    \t\t\t<p>{{ post.summary|raw }}</p>
                                                <a href=\"{{ post.url }}\" class=\"basic-link\">Tovább a hírhez</a>
                                            </div>
                                        </div>
                                        </div>
                                    {% else %}
                                        <div class=\"col-md-12 col-sm-12\">
                                            <div class='alert alert-danger'>{{ noPostsMessage }}</div>
                                        </div>
                                    {% endfor %}
                                </ul>
                        <!-- Page Pagination -->
                            {% if posts.lastPage > 1 %}
                                <nav>
                                    <ul class=\"pagination pagination-lg\">
                                        {% if posts.currentPage > 1 %}
                                            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage-1) }) }}\">&larr; Előző</a></li>
                                        {% endif %}
                                
                                        {% for page in 1..posts.lastPage %}
                                            <li class=\"{{ posts.currentPage == page ? 'active' : null }}\">
                                                <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\">{{ page }}</a>
                                            </li>
                                        {% endfor %}
                                
                                        {% if posts.lastPage > posts.currentPage %}
                                            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (posts.currentPage+1) }) }}\">Következő &rarr;</a></li>
                                        {% endif %}
                                    </nav>
                                {% endif %}
                    </div>
                    <div class=\"col-lg-3 col-md-4\">
                        <div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t{%component 'staticMenu2' %}
                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"{{'/kapcsolat'|app}}\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   \t</div>
{% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/blog.htm", "");
    }
}
