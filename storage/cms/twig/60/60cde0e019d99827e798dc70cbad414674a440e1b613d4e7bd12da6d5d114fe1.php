<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/partials/rmunkasok.htm */
class __TwigTemplate_3584d6acc2a2d8fa268de42891e3c9febb555ef0110299a6ef75a593897ffbfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2>Csapatunk</h2>
                        <p>Ismerje meg munkatársaink.</p>
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"team-slider\" data-columns=\"2\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"2\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team1.jpg");
        echo "\" class=\"magnific-image media-box\"><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team1.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Lengyel Richárd</h3>
                                                <span class=\"meta-data\">CEO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"";
        // line 18
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team2.jpg");
        echo "\" class=\"magnific-image media-box\"><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team2.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Orosz László</h3>
                                                <span class=\"meta-data\">CFO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"";
        // line 28
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team3.jpg");
        echo "\" class=\"magnific-image media-box\"><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/team3.jpg");
        echo "\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Magyar Bence</h3>
                                                <span class=\"meta-data\">CTO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>";
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/rmunkasok.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 28,  43 => 18,  28 => 8,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h2>Csapatunk</h2>
                        <p>Ismerje meg munkatársaink.</p>
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"team-slider\" data-columns=\"2\" data-autoplay=\"\" data-pagination=\"no\" data-arrows=\"yes\" data-single-item=\"no\" data-items-desktop=\"2\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"{{'assets/images/team1.jpg'|theme}}\" class=\"magnific-image media-box\"><img src=\"{{'assets/images/team1.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Lengyel Richárd</h3>
                                                <span class=\"meta-data\">CEO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"{{'assets/images/team2.jpg'|theme}}\" class=\"magnific-image media-box\"><img src=\"{{'assets/images/team2.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Orosz László</h3>
                                                <span class=\"meta-data\">CFO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"team-grid-item grid-item format-image\">
                                            <a href=\"{{'assets/images/team3.jpg'|theme}}\" class=\"magnific-image media-box\"><img src=\"{{'assets/images/team3.jpg'|theme}}\" alt=\"\"></a>
                                            <div class=\"grid-item-inner\">
                                                <h3>Magyar Bence</h3>
                                                <span class=\"meta-data\">CTO</span>
                                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>", "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/rmunkasok.htm", "");
    }
}
