<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/layouts/services.htm */
class __TwigTemplate_66ff20a151cf809198b0d2a264a07227bf56aceae88d66506efa2cbb8d8698b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 2
        echo "    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header\" style=\"background-image:url(";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/page-header5.jpg");
        echo "); background-repeat:no-repeat; background-position:center center;\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\">Főoldal</a></li>
                    <li>Szolgáltatások</li>
                    <li class=\"active\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</li>
                </ol>
            \t<h1>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-md-9 col-sm-8\">
                \t";
        // line 23
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 24
        echo "                   \t</div>
                    <div class=\"col-md-3 col-sm-4\">
                    \t<div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t";
        // line 28
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticMenu2"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 29
        echo "                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"";
        // line 35
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/kapcsolat");
        echo "\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
           \t</div>
        </div>
   \t</div>
   ";
        // line 43
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 43,  78 => 35,  70 => 29,  66 => 28,  60 => 24,  58 => 23,  45 => 13,  40 => 11,  35 => 9,  28 => 5,  23 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% partial 'site/header' %}
    <!-- End Header -->
    <!-- Start Hero Area -->
    <div class=\"hero-area\">
    \t<div class=\"page-header\" style=\"background-image:url({{'assets/images/page-header5.jpg'|theme}}); background-repeat:no-repeat; background-position:center center;\">
        \t<div class=\"container\">
                <!-- Breadcrumb -->
                <ol class=\"breadcrumb\">
                    <li><a href=\"{{''|app}}\">Főoldal</a></li>
                    <li>Szolgáltatások</li>
                    <li class=\"active\">{{ this.page.title }}</li>
                </ol>
            \t<h1>{{ this.page.title }}</h1>
            </div>
        </div>
    </div>
    <!-- Main Content -->
    <div id=\"main-container\">
    \t<div class=\"content\">
        \t<div class=\"container\">
            \t<div class=\"row\">
                \t<div class=\"col-md-9 col-sm-8\">
                \t{% page %}
                   \t</div>
                    <div class=\"col-md-3 col-sm-4\">
                    \t<div class=\"widget sidebar-widget widget_custom_menu\">
                        \t<ul>
                            \t{%component 'staticMenu2' %}
                            </ul>
                        </div>
                    \t<div class=\"widget sidebar-widget text_widget\">
                        \t<div class=\"accent-bg text_banner\">
                            \t<h4>Kérdése van?</h4>
                        \t\t<p>Ügyfélszolgálatunk áll rendelkezésére hétköznaponként munkaidőben.</p>
                                <a href=\"{{'/kapcsolat'|app}}\" class=\"btn btn-default btn-ghost btn-light\">Elérhetőségek</a>
                            </div>
                        </div>
                    </div>
                </div>
           \t</div>
        </div>
   \t</div>
   {% partial 'site/footer' %}", "/Applications/MAMP/htdocs/pannon/themes/pannon/layouts/services.htm", "");
    }
}
