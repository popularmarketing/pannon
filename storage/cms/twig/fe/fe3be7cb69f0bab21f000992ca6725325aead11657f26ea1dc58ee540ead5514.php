<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/footer.htm */
class __TwigTemplate_8605990a25f0620123d5c9000b7134e064e967b21f739f3cb9ef8bdc837c92d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Site Footer -->
    <div class=\"site-footer\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-3 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-info-circle\"></i> Pannon Kft.</h4>
                        <p><img src=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo.png");
        echo "\" alt=\"\"></p>
                        <p>
                            <ul>
                                <li>Színvonalas gazdálkodás</li>
                                <li>Korszerű technikai fejlesztések</li>
                                <li>Átgondolt piackutatás</li>
                                <li>EU szabvány megfelelés</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class=\"col-md-2 col-sm-6\">
                    <div class=\"widget footer_widget widget_links\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-navicon\"></i> Legfontosabb linkek</h4>
                        <nav role=\"menu\">
                        <ul>
                            <li><a href=\"#\">ASZF</a></li>
                            <li><a href=\"#\">Szolgáltatásaink</a></li>
                            <li><a href=\"#\">GYIK</a></li>
                            <li><a href=\"#\">Sajtó</a></li>
                          
                        </ul>
                        </nav>
                    </div>
                </div>
                <div class=\"col-md-3 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-twitter\"></i> Instagram</h4>
                    </div>
                </div>
                <div class=\"col-md-4 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-comment\"></i> Rólunk mondták</h4>
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"1\" data-autoplay=\"\" data-pagination=\"yes\" data-arrows=\"no\" data-single-item=\"no\" data-items-desktop=\"1\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"testimonial-block\">
                                            <blockquote>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                            </blockquote>
                                            <div class=\"testimonial-avatar\"><img src=\"";
        // line 49
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user1.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                            <div class=\"testimonial-info\">
                                                <div class=\"testimonial-info-in\">
                                                    <strong>Kovács Károly és Fia kft</strong><span> Budapest</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"testimonial-block\">
                                            <blockquote>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                            </blockquote>
                                            <div class=\"testimonial-avatar\"><img src=\"";
        // line 62
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/user2.jpg");
        echo "\" alt=\"\" height=\"60\" width=\"60\"></div>
                                            <div class=\"testimonial-info\">
                                                <div class=\"testimonial-info-in\">
                                                    <strong>Kovács Károly és Fia kft</strong><span> Makó</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Site Footer -->
    <div class=\"site-footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-6\">
                    <div class=\"copyrights-col-left\">
                        <p>&copy; ";
        // line 84
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " Popularmarketing Kft</p>
                    </div>
                </div>
                <div class=\"col-md-6 col-sm-6\">
                    <div class=\"copyrights-col-right\">
                        <ul class=\"social-icons-rounded social-icons-colored pull-right\">
                            <li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook-f\"></i></a></li>
             
                            <li class=\"googleplus\"><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
                            <li class=\"youtube\"><a href=\"#\"><i class=\"fa fa-youtube-play\"></i></a></li>
 
                            <li class=\"instagram\"><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id=\"back-to-top\"><i class=\"fa fa-angle-double-up\"></i></a> 
</div>
<script src=\"";
        // line 104
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/jquery-2.1.3.min.js");
        echo "\"></script> <!-- Jquery Library Call -->
<script src=\"";
        // line 105
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/magnific/jquery.magnific-popup.min.js");
        echo "\"></script> <!-- PrettyPhoto Plugin -->
<script src=\"";
        // line 106
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/ui-plugins.js");
        echo "\"></script> <!-- UI Plugins -->
<script src=\"";
        // line 107
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/helper-plugins.js");
        echo "\"></script> <!-- Helper Plugins -->
<script src=\"";
        // line 108
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/owl-carousel/js/owl.carousel.min.js");
        echo "\"></script> <!-- Owl Carousel -->
<script src=\"";
        // line 109
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/bootstrap.js");
        echo "\"></script> <!-- UI -->
<script src=\"";
        // line 110
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/init.js");
        echo "\"></script> <!-- All Scripts -->
<script src=\"";
        // line 111
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/flexslider/js/jquery.flexslider.js");
        echo "\"></script> <!-- FlexSlider -->
<script src=\"";
        // line 112
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/style-switcher/js/jquery_cookie.js");
        echo "\"></script>
<script src=\"";
        // line 113
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/style-switcher/js/script.js");
        echo "\"></script>
<script src=\"";
        // line 114
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script>
        ";
        // line 115
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 116
        echo "        ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 117
        echo "</body>
</html>
<!-- Localized -->";
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 117,  187 => 116,  180 => 115,  176 => 114,  172 => 113,  168 => 112,  164 => 111,  160 => 110,  156 => 109,  152 => 108,  148 => 107,  144 => 106,  140 => 105,  136 => 104,  113 => 84,  88 => 62,  72 => 49,  28 => 8,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Site Footer -->
    <div class=\"site-footer\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-3 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-info-circle\"></i> Pannon Kft.</h4>
                        <p><img src=\"{{'assets/images/logo.png'|theme}}\" alt=\"\"></p>
                        <p>
                            <ul>
                                <li>Színvonalas gazdálkodás</li>
                                <li>Korszerű technikai fejlesztések</li>
                                <li>Átgondolt piackutatás</li>
                                <li>EU szabvány megfelelés</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class=\"col-md-2 col-sm-6\">
                    <div class=\"widget footer_widget widget_links\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-navicon\"></i> Legfontosabb linkek</h4>
                        <nav role=\"menu\">
                        <ul>
                            <li><a href=\"#\">ASZF</a></li>
                            <li><a href=\"#\">Szolgáltatásaink</a></li>
                            <li><a href=\"#\">GYIK</a></li>
                            <li><a href=\"#\">Sajtó</a></li>
                          
                        </ul>
                        </nav>
                    </div>
                </div>
                <div class=\"col-md-3 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-twitter\"></i> Instagram</h4>
                    </div>
                </div>
                <div class=\"col-md-4 col-sm-6\">
                    <div class=\"widget footer_widget\">
                        <h4 class=\"widgettitle\"><i class=\"fa fa-comment\"></i> Rólunk mondták</h4>
                        <div class=\"carousel-wrapper\">
                            <div class=\"row\">
                                <ul class=\"owl-carousel carousel-fw\" id=\"testimonials-slider\" data-columns=\"1\" data-autoplay=\"\" data-pagination=\"yes\" data-arrows=\"no\" data-single-item=\"no\" data-items-desktop=\"1\" data-items-desktop-small=\"1\" data-items-tablet=\"1\" data-items-mobile=\"1\">
                                    <li class=\"item\">
                                        <div class=\"testimonial-block\">
                                            <blockquote>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                            </blockquote>
                                            <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user1.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                            <div class=\"testimonial-info\">
                                                <div class=\"testimonial-info-in\">
                                                    <strong>Kovács Károly és Fia kft</strong><span> Budapest</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class=\"item\">
                                        <div class=\"testimonial-block\">
                                            <blockquote>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                                            </blockquote>
                                            <div class=\"testimonial-avatar\"><img src=\"{{'assets/images/user2.jpg'|theme}}\" alt=\"\" height=\"60\" width=\"60\"></div>
                                            <div class=\"testimonial-info\">
                                                <div class=\"testimonial-info-in\">
                                                    <strong>Kovács Károly és Fia kft</strong><span> Makó</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Site Footer -->
    <div class=\"site-footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-6\">
                    <div class=\"copyrights-col-left\">
                        <p>&copy; {{ \"now\"|date(\"Y\") }} Popularmarketing Kft</p>
                    </div>
                </div>
                <div class=\"col-md-6 col-sm-6\">
                    <div class=\"copyrights-col-right\">
                        <ul class=\"social-icons-rounded social-icons-colored pull-right\">
                            <li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook-f\"></i></a></li>
             
                            <li class=\"googleplus\"><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
                            <li class=\"youtube\"><a href=\"#\"><i class=\"fa fa-youtube-play\"></i></a></li>
 
                            <li class=\"instagram\"><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id=\"back-to-top\"><i class=\"fa fa-angle-double-up\"></i></a> 
</div>
<script src=\"{{'assets/javascript/jquery-2.1.3.min.js'|theme}}\"></script> <!-- Jquery Library Call -->
<script src=\"{{'assets/vendor/magnific/jquery.magnific-popup.min.js'|theme}}\"></script> <!-- PrettyPhoto Plugin -->
<script src=\"{{'assets/javascript/ui-plugins.js'|theme}}\"></script> <!-- UI Plugins -->
<script src=\"{{'assets/javascript/helper-plugins.js'|theme}}\"></script> <!-- Helper Plugins -->
<script src=\"{{'assets/vendor/owl-carousel/js/owl.carousel.min.js'|theme}}\"></script> <!-- Owl Carousel -->
<script src=\"{{'assets/javascript/bootstrap.js'|theme}}\"></script> <!-- UI -->
<script src=\"{{'assets/javascript/init.js'|theme}}\"></script> <!-- All Scripts -->
<script src=\"{{'assets/vendor/flexslider/js/jquery.flexslider.js'|theme}}\"></script> <!-- FlexSlider -->
<script src=\"{{'assets/style-switcher/js/jquery_cookie.js'|theme}}\"></script>
<script src=\"{{'assets/style-switcher/js/script.js'|theme}}\"></script>
<script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
        {% framework extras %}
        {% scripts %}
</body>
</html>
<!-- Localized -->", "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/footer.htm", "");
    }
}
