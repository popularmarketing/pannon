<?php

/* /Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/header.htm */
class __TwigTemplate_eaebc128c9dacfcfeb52d663a2b19e129f333805ca205bec8eabac1bbde81b89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>Pannon Kft. - ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
        <meta name=\"title\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">
        <meta name=\"author\" content=\"Pannon Kft.\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"generator\" content=\"Pannon Kft.\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/october.png");
        echo "\">
        <link href=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/vendor.css");
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/theme.css");
        echo "\" rel=\"stylesheet\">
        <meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0\">
        <meta name=\"format-detection\" content=\"telephone=no\">
        <link href=\"";
        // line 16
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bootstrap.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bootstrap-theme.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 18
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/style.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 19
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/magnific/magnific-popup.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 20
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/owl-carousel/css/owl.carousel.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/owl-carousel/css/owl.theme.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <!--[if lte IE 9]><link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/ie.css");
        echo "\" media=\"screen\" /><![endif]-->
        <link href=\"";
        // line 23
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/custom.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\"><!-- CUSTOM STYLESHEET FOR STYLING -->
        <!-- Color Style -->
        <link class=\"alt\" href=\"";
        // line 25
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/colors/color1.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"";
        // line 26
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/style-switcher/css/style-switcher.css");
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <script src=\"";
        // line 27
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/modernizr.js");
        echo "\"></script><!-- Modernizr -->
        ";
        // line 28
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 29
        echo "    </head>
    <body>
    <!-- Start Header -->
    <div class=\"header-wrapper\">
    \t<header class=\"site-header\">
        \t<div class=\"container\">
                <div class=\"site-logo\">
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\"><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo.png");
        echo "\" alt=\"Logo\"></a>
                </div>
                <div class=\"site-tagline\"></div>
                <a href=\"#\" class=\"btn btn-default btn-sm\" id=\"contact-info\"><i class=\"fa fa-bars\"></i></a>
                <div class=\"site-header-right\">
                \t<ul class=\"header-info-cols\">
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-map-marker\"></i></span>
                            <div><div><span>6500 Baja<br> Petőfi Sándor út 80.</span></div></div>
                        </li>
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-phone\"></i></span>
                            <div><div><span><strong>Itt tud elérni minket</strong><br>+36 79 324-628</span></div></div>
                        </li>
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-clock-o\"></i></span>
                            <div><div><span><strong>Nyitvatartás:</strong><br>Hétfő és Péntek között 8:00-16:00</span></div></div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class=\"main-navigation\">
        \t<div class=\"container\">
            \t<a href=\"";
        // line 60
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/kapcsolat");
        echo "\"  class=\"yelow-bg btn btn-primary btn-sm pull-right quote-head-btn\">Kapcsolat</a>
            \t<ul class=\"pull-right social-icons\">
                \t<li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook-f\"></i></a></li>
                \t<li class=\"youtube\"><a href=\"#\"><i class=\"fa fa-youtube\"></i></a></li>
                \t<li class=\"instagram\"><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                </ul>
                <a href=\"#\" id=\"menu-toggle\">Menu</a>
                <nav role=\"menu\">
                <ul class=\"dd-menu sf-menu\">
                     <li><a href=\"";
        // line 69
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("");
        echo "\">Főoldal</a></li>
                     <li><a href=\"";
        // line 70
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/rolunk");
        echo "\">Rólunk</a></li>
                     <li><a href=\"";
        // line 71
        echo $this->env->getExtension('System\Twig\Extension')->appFilter("/hirek");
        echo "\">Hírek</a></li>

                    <li class=\"megamenu\"><a href=\"javascrip:void(0)\">Szolgáltatásaink</a>
                        <ul class=\"dropdown\">
                            <li>
                                <div class=\"megamenu-container container\">
                                    <div class=\"row\">
                                        <div class=\"col-md-3 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Szolgáltatások</span>
                                            <ul class=\"sub-menu\">
                                                ";
        // line 81
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("staticMenu"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 82
        echo "                                            </ul>
                                        </div>
                                        <div class=\"col-md-4 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Legfrisebb hírek</span>
                                        \t<div class=\"widget recent_posts\">
                                            \t<ul>
                                            \t    ";
        // line 88
        $context["posts"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["blogPosts2"] ?? null), "posts", array());
        // line 89
        echo "                                            \t        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 90
            echo "                                                \t        <li>
                                                    \t        <a href=\"";
            // line 91
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\" class=\"media-box\">";
            if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "featured_images", array()), "count", array())) {
                // line 92
                echo "                                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "featured_images", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 93
                    echo "                                                            <img src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "path", array()), "html", null, true);
                    echo "\" alt=\"image.title != null ? image.title : post.title\">       
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 95
                echo "                                                    ";
            } else {
                // line 96
                echo "                                                        <img src=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img9.jpg");
                echo "\"
                                                        alt=\"\">
                                                    ";
            }
            // line 98
            echo "</a>
                                                \t\t        <h5><a href=\"";
            // line 99
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "title", array()), "html", null, true);
            echo "</a></h5>
                                                \t\t        <span class=\"meta-data grid-item-meta\">";
            // line 100
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "published_at", array()), "Y-M-d"), "html", null, true);
            echo "</span>
                                                            </li>
                                                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 103
            echo "                                                        <li>
                                                           ";
            // line 104
            echo twig_escape_filter($this->env, ($context["noPostsMessage"] ?? null), "html", null, true);
            echo "
                                                        </li>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                                                </ul>
                                            </div>
                                        </div>
                                        <div class=\"col-md-5 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Instagram</span> 
                                        \t<img src=\"";
        // line 112
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/img2.png");
        echo "\" alt=\"\">
                                        </div>
                                    </div>
                                </div>
                            </li>
                      \t</ul>
                  \t</li>
                </ul>
                </nav>
            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 112,  253 => 107,  244 => 104,  241 => 103,  233 => 100,  227 => 99,  224 => 98,  217 => 96,  214 => 95,  205 => 93,  200 => 92,  196 => 91,  193 => 90,  187 => 89,  185 => 88,  177 => 82,  173 => 81,  160 => 71,  156 => 70,  152 => 69,  140 => 60,  111 => 36,  102 => 29,  99 => 28,  95 => 27,  91 => 26,  87 => 25,  82 => 23,  78 => 22,  74 => 21,  70 => 20,  66 => 19,  62 => 18,  58 => 17,  54 => 16,  48 => 13,  44 => 12,  40 => 11,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>Pannon Kft. - {{ this.page.title }}</title>
        <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
        <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
        <meta name=\"author\" content=\"Pannon Kft.\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"generator\" content=\"Pannon Kft.\">
        <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/october.png'|theme }}\">
        <link href=\"{{ 'assets/css/vendor.css'|theme }}\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/theme.css'|theme }}\" rel=\"stylesheet\">
        <meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0\">
        <meta name=\"format-detection\" content=\"telephone=no\">
        <link href=\"{{'assets/css/bootstrap.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/css/bootstrap-theme.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/css/style.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/vendor/magnific/magnific-popup.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/vendor/owl-carousel/css/owl.carousel.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/vendor/owl-carousel/css/owl.theme.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <!--[if lte IE 9]><link rel=\"stylesheet\" type=\"text/css\" href=\"{{'assets/css/ie.css'|theme}}\" media=\"screen\" /><![endif]-->
        <link href=\"{{'assets/css/custom.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\"><!-- CUSTOM STYLESHEET FOR STYLING -->
        <!-- Color Style -->
        <link class=\"alt\" href=\"{{'assets/colors/color1.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <link href=\"{{'assets/style-switcher/css/style-switcher.css'|theme}}\" rel=\"stylesheet\" type=\"text/css\">
        <script src=\"{{'assets/javascript/modernizr.js'|theme}}\"></script><!-- Modernizr -->
        {% styles %}
    </head>
    <body>
    <!-- Start Header -->
    <div class=\"header-wrapper\">
    \t<header class=\"site-header\">
        \t<div class=\"container\">
                <div class=\"site-logo\">
                    <a href=\"{{''|app}}\"><img src=\"{{'assets/images/logo.png'|theme}}\" alt=\"Logo\"></a>
                </div>
                <div class=\"site-tagline\"></div>
                <a href=\"#\" class=\"btn btn-default btn-sm\" id=\"contact-info\"><i class=\"fa fa-bars\"></i></a>
                <div class=\"site-header-right\">
                \t<ul class=\"header-info-cols\">
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-map-marker\"></i></span>
                            <div><div><span>6500 Baja<br> Petőfi Sándor út 80.</span></div></div>
                        </li>
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-phone\"></i></span>
                            <div><div><span><strong>Itt tud elérni minket</strong><br>+36 79 324-628</span></div></div>
                        </li>
  \t\t\t\t\t\t<li>
                        \t<span class=\"icon-col\"><i class=\"fa fa-clock-o\"></i></span>
                            <div><div><span><strong>Nyitvatartás:</strong><br>Hétfő és Péntek között 8:00-16:00</span></div></div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class=\"main-navigation\">
        \t<div class=\"container\">
            \t<a href=\"{{'/kapcsolat'|app}}\"  class=\"yelow-bg btn btn-primary btn-sm pull-right quote-head-btn\">Kapcsolat</a>
            \t<ul class=\"pull-right social-icons\">
                \t<li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook-f\"></i></a></li>
                \t<li class=\"youtube\"><a href=\"#\"><i class=\"fa fa-youtube\"></i></a></li>
                \t<li class=\"instagram\"><a href=\"#\"><i class=\"fa fa-instagram\"></i></a></li>
                </ul>
                <a href=\"#\" id=\"menu-toggle\">Menu</a>
                <nav role=\"menu\">
                <ul class=\"dd-menu sf-menu\">
                     <li><a href=\"{{''|app}}\">Főoldal</a></li>
                     <li><a href=\"{{'/rolunk'|app}}\">Rólunk</a></li>
                     <li><a href=\"{{'/hirek'|app}}\">Hírek</a></li>

                    <li class=\"megamenu\"><a href=\"javascrip:void(0)\">Szolgáltatásaink</a>
                        <ul class=\"dropdown\">
                            <li>
                                <div class=\"megamenu-container container\">
                                    <div class=\"row\">
                                        <div class=\"col-md-3 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Szolgáltatások</span>
                                            <ul class=\"sub-menu\">
                                                {% component 'staticMenu' %}
                                            </ul>
                                        </div>
                                        <div class=\"col-md-4 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Legfrisebb hírek</span>
                                        \t<div class=\"widget recent_posts\">
                                            \t<ul>
                                            \t    {% set posts = blogPosts2.posts %}
                                            \t        {% for post in posts %}
                                                \t        <li>
                                                    \t        <a href=\"{{ post.url }}\" class=\"media-box\">{% if post.featured_images.count %}
                                                        {% for image in post.featured_images|slice(0, 1) %}
                                                            <img src=\"{{image.path}}\" alt=\"image.title != null ? image.title : post.title\">       
                                                        {% endfor %}
                                                    {% else %}
                                                        <img src=\"{{'assets/images/img9.jpg'|theme}}\"
                                                        alt=\"\">
                                                    {% endif %}</a>
                                                \t\t        <h5><a href=\"{{ post.url }}\">{{ post.title }}</a></h5>
                                                \t\t        <span class=\"meta-data grid-item-meta\">{{ post.published_at|date('Y-M-d') }}</span>
                                                            </li>
                                                    {% else %}
                                                        <li>
                                                           {{ noPostsMessage }}
                                                        </li>
                                                    {% endfor %}
                                                </ul>
                                            </div>
                                        </div>
                                        <div class=\"col-md-5 megamenu-col\">
                                        \t<span class=\"megamenu-sub-title\">Instagram</span> 
                                        \t<img src=\"{{'assets/images/img2.png'|theme}}\" alt=\"\">
                                        </div>
                                    </div>
                                </div>
                            </li>
                      \t</ul>
                  \t</li>
                </ul>
                </nav>
            </div>
        </div>
    </div>", "/Applications/MAMP/htdocs/pannon/themes/pannon/partials/site/header.htm", "");
    }
}
